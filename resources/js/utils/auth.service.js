import decode from 'jwt-decode'
import axios from 'axios'
import Vue from "vue";

export function logoutUser() {
    clearAuthToken()
}

export function setAuthToken(data) {
    Vue.$cookies.set(
        process.env.MIX_API_TOKEN_NAME,
        data.access_token,
        data.expires_in,
        "/",
        process.env.MIX_COOKIE_DOMAIN,
        false,
        null
    );
    axios.defaults.headers.common['Authorization'] = `Bearer ${data.access_token}`
}

export function getAuthToken() {
    return Vue.$cookies.get(process.env.MIX_API_TOKEN_NAME);
}

export function clearAuthToken() {
    axios.defaults.headers.common['Authorization'] = ''
}

export function isLoggedIn() {
    let authToken = getAuthToken()
    return !!authToken && !isTokenExpired(authToken)
}

function getTokenExpirationDate(encodedToken) {
    let token = decode(encodedToken)
    if (!token.exp) {
        return null
    }

    let date = new Date(0)
    date.setUTCSeconds(token.exp)

    return date
}

function isTokenExpired(token) {
    let expirationDate = getTokenExpirationDate(token)
    return expirationDate < new Date()
}
