<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite as FacadesSocialite;
use Socialite;

class AuthController extends Controller
{


  public function __construct()
  {
  }


  public function SocialSignup($provider)
  {
    $user = FacadesSocialite::driver($provider)->stateless()->user();
    return response()->json($user);
  }
}
